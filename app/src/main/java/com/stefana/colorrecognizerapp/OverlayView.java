package com.stefana.colorrecognizerapp;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.view.MotionEvent;

import java.util.logging.Handler;

public class OverlayView extends AppCompatTextView implements ColorListener {

    public static final int SIZE = 40;

    public OverlayView(Context context) {
        super(context);
        p.setColor(Color.GREEN);
    }

    public OverlayView(Context context, AttributeSet attrs) {
        super(context, attrs);
        p.setColor(Color.GREEN);
    }

    public OverlayView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        p.setColor(Color.GREEN);
    }

    @Override
    public void setColor(int color) {
        p.setColor(color);
        invalidate();
    }

    class Point {
        int X;
        int Y;

        public Point() {
            X = Y = -1;
        }

    }

    private Point mCurPoint = null;
    private Paint p = new Paint();


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (mCurPoint != null) {
            canvas.drawCircle(mCurPoint.X, mCurPoint.Y, SIZE, p);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            mCurPoint = new Point();
            mCurPoint.X = (int) event.getX();
            mCurPoint.Y = (int) event.getY();
            invalidate();
            new android.os.Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mCurPoint = null;
                    invalidate();
                }
            }, 2000);
        }
        super.onTouchEvent(event);
        return false;
    }

}
