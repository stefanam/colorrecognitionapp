package com.stefana.colorrecognizerapp;


import android.graphics.Color;

public class ColorEntry {

    public int color;
    public String name;

    public ColorEntry(String name, String hexCode) {
        this.name = name;
        this.color = Color.parseColor(hexCode);
    }
}
