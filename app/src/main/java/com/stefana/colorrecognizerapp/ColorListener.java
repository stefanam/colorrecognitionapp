package com.stefana.colorrecognizerapp;

interface ColorListener {

    void setColor(int color);

}
