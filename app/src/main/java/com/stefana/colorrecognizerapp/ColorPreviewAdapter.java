package com.stefana.colorrecognizerapp;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

public class ColorPreviewAdapter extends RecyclerView.Adapter<ViewHolder> {

    private ArrayList<ColorEntry> colorEntries = new ArrayList<>();

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.color_preview_view_holder, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ColorEntry colorEntry = colorEntries.get(position);
        holder.colorName.setText(colorEntry.name);
        holder.colorShapeView.setBackgroundColor(colorEntry.color);
    }

    @Override
    public int getItemCount() {
        return colorEntries.size();
    }

    public void setColorEntries(ArrayList<ColorEntry> colorEntries) {
        this.colorEntries = colorEntries;
    }
}
