package com.stefana.colorrecognizerapp;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.View;
import android.widget.Toast;

import com.hardik.screencolorreader_android.ScreenColorPicker;

import java.util.Collections;
import java.util.Locale;

public class PrimeActivity extends AppCompatActivity implements TextToSpeech.OnInitListener {

    private static final int COLOR_NO = 139;

    protected ScreenColorPicker screenColorPicker;
    protected ColorEntry[] colors = new ColorEntry[COLOR_NO];
    protected TextToSpeech tts;

    public void initialize() {

        tts = new TextToSpeech(getApplicationContext(), this);
        tts.setLanguage(Locale.US);

        populateColors();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        screenColorPicker = new ScreenColorPicker(this, size.x, size.y);
        screenColorPicker.initialise();
        screenColorPicker.checkPermission(new ScreenColorPicker.PermissionCallbacks() {
            @Override
            public void onPermissionGranted() {
                //add code to be performed when permission is granted.
                System.out.println("Granted");
            }

            @Override
            public void onPermissionDenied() {
                //add code to be performed when permission is denied.
                System.out.println("Denied");
            }
        }, 100, RESULT_OK, null);
    }

    public void previewColor(final int x, final int y) {

        Thread thread = new Thread() {
            @Override
            public void run() {
                super.run();
                try {

                    final int color = screenColorPicker.getColorInt(x, y);
                    setColor(color);
                    final ColorEntry similarColor = getBestMatchingColor(color);
                    if (tts != null)
                        tts.speak(similarColor.name, TextToSpeech.QUEUE_ADD, null, null);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast toastc = Toast.makeText(PrimeActivity.this, similarColor.name, Toast.LENGTH_LONG);

                            View view = toastc.getView();
                            view.getBackground().setColorFilter(color, PorterDuff.Mode.SRC_IN);
                            toastc.show();
                        }
                    });


                } catch (Exception e) {
                    e.printStackTrace();
                    //Toast.makeText(PrimeActivity.this, "Error loading colorsth", Toast.LENGTH_SHORT).show();
                }
            }
        };
        thread.start();

    }

    private ColorEntry getBestMatchingColor(int color) {
        int[] similarities = new int[COLOR_NO];
        int i = 0;
        //izracunavamo slicnost boje sa svim zakucanim bojama
        for (ColorEntry entry : colors) {
            similarities[i++] = calculateSImilarity(entry.color, color);
        }
        //trazimo najmanju razliku boja
        int indexWithMin = 0;
        int minValue = Integer.MAX_VALUE;
        i = 0;
        for (int sim : similarities) {
            if (sim < minValue) {
                indexWithMin = i;
                minValue = sim;
            }
            i++;
        }
        return colors[indexWithMin];
    }


    public int calculateSImilarity(int first, int second) {
        return Math.abs(Color.red(second) - Color.red(first))
                + Math.abs(Color.green(second) - Color.green(first))
                + Math.abs(Color.blue(second) - Color.blue(first));
    }

    public int getColorForMultiplePixels(int x, int y) {

        int colorA = screenColorPicker.getColorInt(x, y);
        int colorB = screenColorPicker.getColorInt(x + 1, y);
        int colorC = screenColorPicker.getColorInt(x, y + 1);
        int colorD = screenColorPicker.getColorInt(x + 1, y + 1);
        int colorE = screenColorPicker.getColorInt(x - 1, y);
        int colorF = screenColorPicker.getColorInt(x, y - 1);
        int colorG = screenColorPicker.getColorInt(x - 1, y - 1);
        int colorH = screenColorPicker.getColorInt(x + 1, y - 1);
        int colorI = screenColorPicker.getColorInt(x - 1, y + 1);

        return getMediumColor(colorA, colorB, colorC, colorD, colorE, colorF, colorG, colorH, colorI);
    }

    public int getMediumColor(int... colors) {
        int simRed = 0, simGreen = 0, simBlue = 0;

        for (int entry : colors) {
            simRed += Math.abs(Color.red(entry));
            simGreen += Math.abs(Color.green(entry));
            simBlue += Math.abs(Color.blue(entry));
        }
        return Color.rgb(simRed / colors.length, simGreen / colors.length, simBlue / colors.length);


    }

    @Override
    protected void onStart() {
        super.onStart();
        if (screenColorPicker != null)
            screenColorPicker.start();

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (screenColorPicker != null)
            screenColorPicker.stop();
        if (tts != null)
            tts.stop();
        tts = null;
    }

    public void setColor(int color) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        screenColorPicker.checkPermission(new ScreenColorPicker.PermissionCallbacks() {
            @Override
            public void onPermissionGranted() {
                screenCaptureGranted();
            }

            @Override
            public void onPermissionDenied() {
                finish();
            }
        }, requestCode, resultCode, data);
    }

    public void screenCaptureGranted() {

    }

    public void populateColors() {

        colors[0] = new ColorEntry("Orange", "#FF8000");
        colors[1] = new ColorEntry("Red", "#FF0000");
        colors[2] = new ColorEntry("Yellow", "#FFFF00");
        colors[3] = new ColorEntry("Green", "#00FF00");
        colors[4] = new ColorEntry("Blue", "#0000FF");
        colors[5] = new ColorEntry("Magenta", "#FF00FF");
        colors[6] = new ColorEntry("Pink", "#FF3399");
        colors[7] = new ColorEntry("Gray", "#808080");
        colors[8] = new ColorEntry("Black", "#000000");
        colors[9] = new ColorEntry("White", "#FFFFFF");
        colors[10] = new ColorEntry("Silver", "#C0C0C0");
        colors[11] = new ColorEntry("Maroon", "#800000");
        colors[12] = new ColorEntry("Olive", "#808000");
        colors[13] = new ColorEntry("Teal", "#008080");
        colors[14] = new ColorEntry("Dark Purple", "#800080");
        colors[15] = new ColorEntry("Navy", "#000080");
        colors[16] = new ColorEntry("Brown", "#A52A2A");
        colors[17] = new ColorEntry("Purple", "#FF00FF");
        colors[18] = new ColorEntry("Aqua", "#00FFFF");

        colors[19] = new ColorEntry("Medium turquoise", "#48D1CC");
        colors[20] = new ColorEntry("Azure blue", "#F0FFFF");
        colors[21] = new ColorEntry("Mint", "#F0FFF0");
        colors[22] = new ColorEntry("Lavender", "#E6E6FA");
        colors[23] = new ColorEntry("Light Steal Blue", "#B0C4DE");
        colors[24] = new ColorEntry("Papaya", "#FFEFD5");
        colors[25] = new ColorEntry("Peach", "#FFDAB9");
        colors[26] = new ColorEntry("Rosy brown", "#BC8F8F");
        colors[27] = new ColorEntry("Tan", "#D2B48C");
        colors[28] = new ColorEntry("Chocolate", "#D2691E");
        colors[29] = new ColorEntry("Light yellow", "#FFFFE0");
        colors[30] = new ColorEntry("Beige", "#F5F5DC");
        colors[31] = new ColorEntry("Light Pink", "#FFB6C1");
        colors[32] = new ColorEntry("Hot pink", "#FF69B4");
        colors[33] = new ColorEntry("Deep pink", "#FF1493");
        colors[34] = new ColorEntry("Pale violet red", "#DB7093");
        colors[35] = new ColorEntry("Medium violet red", "#C71585");
        colors[36] = new ColorEntry("Violet", "#EE82EE");
        colors[37] = new ColorEntry("Dark violet", "#9400D3");

        colors[38] = new ColorEntry("Thistle", "#D8BFD8");
        colors[39] = new ColorEntry("Light blue", "#ADD8E6");
        colors[40] = new ColorEntry("Sky blue", "#87CEEB");
        colors[41] = new ColorEntry("Steal Blue", "#4682B4");
        colors[42] = new ColorEntry("Cadet blue", "#5F9EA0");
        colors[43] = new ColorEntry("Turquoise", "#40E0D0");
        colors[44] = new ColorEntry("Light sea green", "#20B2AA");
        colors[45] = new ColorEntry("Medium sea green", "#3CB371");
        colors[46] = new ColorEntry("Spring green", "#00FF7F");
        colors[47] = new ColorEntry("Dark sea green", "#8FBC8F");
        colors[48] = new ColorEntry("Pale green", "#98FB98");
        colors[49] = new ColorEntry("Forest green", "#228B22");
        colors[50] = new ColorEntry("Yellow green", "#9ACD32");
        colors[51] = new ColorEntry("Gold", "#FFD700");
        colors[52] = new ColorEntry("Golden rod", "#DAA520");
        colors[53] = new ColorEntry("Dark orange", "#FF8C00");
        colors[54] = new ColorEntry("Salmon", "#FA8072");
        colors[55] = new ColorEntry("Coral", "#FF7F50");
        colors[56] = new ColorEntry("Dim gray", "#696969");

        colors[57] = new ColorEntry("Blue Violet", "#8A2BE2");
        colors[58] = new ColorEntry("Dark Violet", "#9400D3");
        colors[59] = new ColorEntry("Light Salmon", "#FFA07A");
        colors[60] = new ColorEntry("Dark Salmon", "#E9967A");
        colors[61] = new ColorEntry("Light Coral", "#F08080");
        colors[62] = new ColorEntry("Indian Red", "#CD5C5C");
        colors[63] = new ColorEntry("Crimson", "#DC143C");
        colors[64] = new ColorEntry("Fire Brick", "#B22222");
        colors[65] = new ColorEntry("Dark Red", "#8B0000");
        colors[66] = new ColorEntry("Orange Red", "#FF4500");
        colors[67] = new ColorEntry("Yellow Chiffon", "#FFFACD");
        colors[68] = new ColorEntry("Light Goldenrod Yellow", "#FAFAD2");
        colors[69] = new ColorEntry("Papaya Whip", "#FFEFD5");
        colors[70] = new ColorEntry("Moccasin", "#FFE4B5");
        colors[71] = new ColorEntry("Peach Puff", "#FFDAB9");
        colors[72] = new ColorEntry("Pale Goldenrod", "#EEE8AA");
        colors[73] = new ColorEntry("Khaki", "#F0E68C");
        colors[74] = new ColorEntry("Dark Khaki", "#BDB76B");
        colors[75] = new ColorEntry("Cornsilk", "#FFF8DC");

        colors[76] = new ColorEntry("Blanched Almond", "#FFEBCD");
        colors[77] = new ColorEntry("Bisque", "#FFE4C4");
        colors[78] = new ColorEntry("Navajo White", "#FFDEAD");
        colors[79] = new ColorEntry("Wheat", "#F5DEB3");
        colors[80] = new ColorEntry("Burly Wood", "#DEB887");
        colors[81] = new ColorEntry("Sandy Brown", "#F4A460");
        colors[82] = new ColorEntry("Dark Goldenrod", "#B8860B");
        colors[83] = new ColorEntry("Peru", "#CD853F");
        colors[84] = new ColorEntry("Saddle Brown", "#8B4513");
        colors[85] = new ColorEntry("Sienna", "#A0522D");
        colors[86] = new ColorEntry("Dark Olive Green", "#556B2F");
        colors[87] = new ColorEntry("Lime Green", "#32CD32");
        colors[88] = new ColorEntry("Lawn Green", "#7CFC00");
        colors[89] = new ColorEntry("Chartreuse", "#7FFF00");
        colors[90] = new ColorEntry("Medium Spring Green ", "#00FA9A");
        colors[91] = new ColorEntry("Light Green", "#90EE90");
        colors[92] = new ColorEntry("Medium Aquamarine", "#66CDAA");
        colors[93] = new ColorEntry("Dark Green", "#006400");
        colors[94] = new ColorEntry("Grass Green", "#008000");

        colors[95] = new ColorEntry("Light Cyan", "#E0FFFF");
        colors[96] = new ColorEntry("Pale Turquoise", "#AFEEEE");
        colors[97] = new ColorEntry("Aquamarine", "#7FFFD4");
        colors[98] = new ColorEntry("Dark Turquoise", "#00CED1");
        colors[99] = new ColorEntry("Dark Cyan", "#008B8B");
        colors[100] = new ColorEntry("Powder Blue", "#B0E0E6");
        colors[101] = new ColorEntry("Light Sky Blue", "#87CEFA");
        colors[102] = new ColorEntry("Deep Sky Blue", "#00BFFF");
        colors[103] = new ColorEntry("Dodger Blue", "#1E90FF");
        colors[104] = new ColorEntry("Cornflower Blue", "#6495ED");
        colors[105] = new ColorEntry("Royal Blue", "#4169E1");
        colors[106] = new ColorEntry("Medium Blue", "#0000CD");
        colors[107] = new ColorEntry("Dark Blue", "#00008B");
        colors[108] = new ColorEntry("Lavender", "#E6E6FA");
        colors[109] = new ColorEntry("Plum", "#DDA0DD");
        colors[110] = new ColorEntry("Orchid", "#DA70D6");
        colors[111] = new ColorEntry("Fuchsia", "#FF00FF");
        colors[112] = new ColorEntry("Medium Orchid", "#BA55D3");
        colors[113] = new ColorEntry("Dark Orchid", "#9932CC");

        colors[114] = new ColorEntry("Dark Magenta", "#8B008B");
        colors[115] = new ColorEntry("Indigo", "#4B0082");
        colors[116] = new ColorEntry("Dark Slate Blue", "#483D8B");
        colors[117] = new ColorEntry("Slate Blue", "#6A5ACD");
        colors[118] = new ColorEntry("Medium Slate Blue", "#7B68EE");
        colors[119] = new ColorEntry("Snow", "#FFFAFA");
        colors[120] = new ColorEntry("Honeydew", "#F0FFF0");
        colors[121] = new ColorEntry("Mint Cream", "#F5FFFA");
        colors[122] = new ColorEntry("Azure", "#F0FFFF");
        colors[123] = new ColorEntry("Alice Blue", "#F0F8FF");
        colors[124] = new ColorEntry("Ghost White", "#F8F8FF");
        colors[125] = new ColorEntry("White Smoke", "#F5F5F5");
        colors[126] = new ColorEntry("Seashell", "#FFF5EE");
        colors[127] = new ColorEntry("Old Lace", "#FDF5E6");
        colors[128] = new ColorEntry("Floral White", "#FFFAF0");
        colors[129] = new ColorEntry("Ivory", "#FFFFF0");
        colors[130] = new ColorEntry("Antique White", "#FAEBD7");
        colors[131] = new ColorEntry("Linen", "#FAF0E6");
        colors[132] = new ColorEntry("Misty Rose", "#FFE4E1");

        colors[133] = new ColorEntry("Gainsboro", "#DCDCDC");
        colors[134] = new ColorEntry("Light Gray", "#D3D3D3");
        colors[135] = new ColorEntry("Dark Gray", "#A9A9A9");
        colors[136] = new ColorEntry("Light Slate Gray", "#778899");
        colors[137] = new ColorEntry("Slate Gray", "#708090");
        colors[138] = new ColorEntry("Dark Slate Gray", "#2F4F4F");
        //itd
    }

    @Override
    public void onInit(int status) {
        System.out.println("TTS: " + status);
    }

    @Override
    protected void onDestroy() {
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
    }

}
