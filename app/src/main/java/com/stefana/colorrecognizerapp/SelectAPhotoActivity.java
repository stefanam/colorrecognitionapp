package com.stefana.colorrecognizerapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectAPhotoActivity extends PrimeActivity {

    private static final int PICK_IMAGE = 100;
    ImageMagnifier imageView;
    FrameLayout frameLayout;
    @BindView(R.id.overlay)
    OverlayView overlayView;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_photo);
        ButterKnife.bind(this);
        initialize();


        imageView = findViewById(R.id.imageView);
        frameLayout = findViewById(R.id.frame);

        openGalery();
        imageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int x = (int) event.getX();
                int y = (int) event.getY();

                if (event.getAction() == MotionEvent.ACTION_UP)
                    previewColor(x, y);
                Log.e("stefana", "koordinate --> " + x + "," + y);
                return true;

            }
        });

    }


    @Override
    protected void onRestart() {
        super.onRestart();
        initialize();
    }

    @Override
    public void setColor(int color) {
        overlayView.setColor(color);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                // ImageView imageView = findViewById(R.id.imageView);
                ImageMagnifier imageMagnifier = findViewById(R.id.imageView);
                imageMagnifier.setImageBitmap(bitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public void openGalery() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
    }

    @Override
    protected void onStart() {
        initialize();
        super.onStart();
        ButterKnife.bind(this);

    }


}
