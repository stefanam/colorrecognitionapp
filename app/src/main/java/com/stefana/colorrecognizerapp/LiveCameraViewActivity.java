package com.stefana.colorrecognizerapp;

import android.Manifest;
import android.app.ActionBar;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.pm.PackageManager;

import com.google.android.cameraview.CameraView;

import android.graphics.Color;
import android.graphics.Point;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LiveCameraViewActivity extends PrimeActivity {

    // Button findColor;
    private static final int PERMISSION_REQUEST_CODE = 200;
    private CameraView view;
    private android.support.v7.widget.Toolbar toolbar;
    @BindView(R.id.overlay)
    OverlayView overlayView;


    @Override
    protected void onRestart() {
        super.onRestart();
        initialize();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.live_camera_view);
        ButterKnife.bind(this);
        initialize();

        setSupportActionBar((android.support.v7.widget.Toolbar) findViewById(R.id.toolBar));


//        findColor = findViewById(R.id.colorButton);
//
//        findColor.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//            }
//        });

        view = findViewById(R.id.camera);
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int x = (int) event.getX();
                int y = (int) event.getY();

                if (event.getAction() == MotionEvent.ACTION_UP)
                    previewColor(x, y);
                return true;
            }
        });

    }

    @Override
    public void setColor(int color) {
        overlayView.setColor(color);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getItemId() == R.id.action_find_color) {
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);

            previewColor(size.x / 2, size.y / 2);

        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onStart() {
        super.onStart();
        initialize();

        ButterKnife.bind(this);
        view.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        view.stop();
    }

    @Override
    public void screenCaptureGranted() {
        view.start();
    }

}

