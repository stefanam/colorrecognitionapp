package com.stefana.colorrecognizerapp;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class ViewHolder extends RecyclerView.ViewHolder {

    public TextView colorShapeView;
    public TextView color1;
    public TextView colorName;

    public ViewHolder(View itemView) {
        super(itemView);
        colorShapeView = itemView.findViewById(R.id.circle);
        color1 = itemView.findViewById(R.id.color1);
        colorName = itemView.findViewById(R.id.color_name);
    }
}
